{{ template "header.tpl" . }}

var extendedPictographicMin rune = {{ printf "0x%08X" .Min }}
var extendedPictographicMax rune = {{ printf "0x%08X" .Max }}

var extendedPictographic = map[uint32]uint64{
	{{ $every := every 4 }}
{{- range $key, $value := .BitField }}
{{- if ne $value 0 }}
{{- printf "0x%08X: 0x%016X" $key $value }}, {{ if $every.Test }}
	{{ end }}
{{- end }}
{{- end }}
