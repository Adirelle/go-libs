package ansi_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adirelle/go-libs/ansi"
)

type styleTestCase struct {
	modifiers []ansi.StyleModifier

	expBlank  bool
	expString string
	expSprint string
}

func makeStyleTestCase(expBlank bool, expString, expSprint string, modifiers ...ansi.StyleModifier) styleTestCase {
	return styleTestCase{modifiers, expBlank, expString, expSprint}
}

func makeBlankStyleTestCase(modifiers ...ansi.StyleModifier) styleTestCase {
	return styleTestCase{modifiers, true, "\x1b[m", "foobar"}
}

func (c styleTestCase) String() string {
	return fmt.Sprintf("case(%v,%v,%q,%q)", c.modifiers, c.expBlank, c.expString, c.expSprint)
}

func (c styleTestCase) Run(t *testing.T) {
	assert := assert.New(t)

	style := &ansi.Style{}

	style.Apply(c.modifiers...)

	assert.Equal(c.expBlank, style.IsBlank())
	assert.Equal(c.expString, style.String())
	assert.Equal(c.expSprint, style.Sprint("foobar"))
}

func TestStyle(t *testing.T) {
	RunSubTestv(t,
		makeStyleTestCase(false, "\x1b[1m", "\x1b[1mfoobar\x1b[m", ansi.Bold),
		makeStyleTestCase(false, "\x1b[1m", "\x1b[1mfoobar\x1b[m", ansi.Faint, ansi.Bold),
		makeStyleTestCase(false, "\x1b[1;3m", "\x1b[1;3mfoobar\x1b[m", ansi.Bold, ansi.Italic),
		makeStyleTestCase(false, "\x1b[30m", "\x1b[30mfoobar\x1b[m", ansi.FgBlue, ansi.FgBlack),

		makeBlankStyleTestCase(),
		makeBlankStyleTestCase(ansi.Bold, ansi.Italic, ansi.FgWhite, ansi.BgBlack, ansi.Reset),
		makeBlankStyleTestCase(ansi.Bold, ansi.ResetWeight),
		makeBlankStyleTestCase(ansi.Faint, ansi.ResetWeight),
		makeBlankStyleTestCase(ansi.Italic, ansi.ResetItalic),
		makeBlankStyleTestCase(ansi.Underline, ansi.ResetUnderline),
		makeBlankStyleTestCase(ansi.SlowBlink, ansi.ResetBlink),
		makeBlankStyleTestCase(ansi.RapidBlink, ansi.ResetBlink),
		makeBlankStyleTestCase(ansi.Reverse, ansi.ResetReverse),
		makeBlankStyleTestCase(ansi.Conceal, ansi.ResetConceal),
		makeBlankStyleTestCase(ansi.Strike, ansi.ResetStrike),
		makeBlankStyleTestCase(ansi.FgWhite, ansi.ResetForeground),
		makeBlankStyleTestCase(ansi.BgWhite, ansi.ResetBackground),
	)
}

func TestExtendedColors(t *testing.T) {
	t.Run("indexed foreground", func(t *testing.T) {
		assert.Equal(t, "38;5;4", ansi.FgIndexedColor(4).String())
	})
	t.Run("RGB foreground", func(t *testing.T) {
		assert.Equal(t, "38;2;128;5;10", ansi.FgRGBColor(128, 5, 10).String())
	})
	t.Run("indexed background", func(t *testing.T) {
		assert.Equal(t, "48;5;4", ansi.BgIndexedColor(4).String())
	})
	t.Run("RGB background", func(t *testing.T) {
		assert.Equal(t, "48;2;128;5;10", ansi.BgRGBColor(128, 5, 10).String())
	})
}

type applyAndFilterTestCase struct {
	modifiers []ansi.StyleModifier

	expStyle     string
	expModifiers string
}

func makeApplyAndFilterTestCase(expStyle, expModifiers string, modifiers ...ansi.StyleModifier) applyAndFilterTestCase {
	return applyAndFilterTestCase{modifiers, expStyle, expModifiers}
}

func (c applyAndFilterTestCase) String() string {
	return fmt.Sprintf("case(%v,%q,%q)", c.modifiers, c.expStyle, c.expModifiers)
}

func (c applyAndFilterTestCase) Run(t *testing.T) {
	assert := assert.New(t)

	style := &ansi.Style{}

	added := style.ApplyAndFilter(c.modifiers...)

	assert.Equal(c.expStyle, style.String())
	assert.Equal(c.expModifiers, join(added))
}

func TestApplyAndFilter(t *testing.T) {
	RunSubTestv(t,
		makeApplyAndFilterTestCase("\x1b[1m", "1", ansi.Bold),
		makeApplyAndFilterTestCase("\x1b[1m", "1", ansi.Bold, ansi.Bold),
		makeApplyAndFilterTestCase("\x1b[1;3m", "1;3", ansi.Bold, ansi.Italic),
		makeApplyAndFilterTestCase("\x1b[m", "22", ansi.Bold, ansi.ResetWeight),
		makeApplyAndFilterTestCase("\x1b[m", "0", ansi.Bold, ansi.Reset),
	)
}
