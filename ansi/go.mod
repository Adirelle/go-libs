module gitlab.com/adirelle/go-libs/ansi

go 1.19

require (
	github.com/leaanthony/go-ansi-parser v1.6.0
	github.com/rivo/uniseg v0.3.4
	github.com/stretchr/testify v1.8.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
