{{ template "header.tpl" . }}

var clusterBoundaryProperties = clusterBoundaryPropertyMap{
	EOTRune: eot,
	{{ $every := every 5 }}
{{- range $key, $value := .Properties }}
{{- printf "0x%05X" $key }}: {{ $value }}, {{ if $every.Test }}
	{{ end -}}
{{- end -}}
}
