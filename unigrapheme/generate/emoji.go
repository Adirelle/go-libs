package main

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"log"
	"unicode/utf8"
)

type (
	EmojiData struct {
		FetchInfo
		Package       string
		Min           rune
		Max           rune
		ExtendedPicto map[rune]bool
		BitField      []uint64
	}
)

func readEmojiDataFrom(ctx context.Context, url string) (d EmojiData, err error) {
	var input io.ReadCloser
	if d.FetchInfo, input, err = fetch(ctx, url); err != nil {
		return
	}
	defer input.Close()

	d.Package = "unigrapheme"
	d.Min = utf8.MaxRune
	d.ExtendedPicto = make(map[rune]bool, utf8.MaxRune)

	lines := bufio.NewScanner(input)
	lineCount := 0
	for lines.Scan() {
		lineCount++
		var from, to rune

		if n, err := fmt.Sscanf(lines.Text(), "%x..%x ; Extended_Pictographic#", &from, &to); err == nil && n == 2 {
			// OK
		} else if n, err := fmt.Sscanf(lines.Text(), "%x ; Extended_Pictographic#", &from); err == nil && n == 1 {
			to = from
		} else {
			continue
		}

		for ; from <= to; from++ {
			if from < d.Min {
				d.Min = from
			}
			if from > d.Max {
				d.Max = from
			}
			d.ExtendedPicto[from] = true
		}
	}

	log.Printf("read %d lines, found %d extended pictography", lineCount, len(d.ExtendedPicto))

	return
}

/*
func writeEmojiDataTo(path string, d EmojiData) error {
	storageWidth := 6

	var bitMask uint32 = (1 << storageWidth) - 1
	var offsetMask uint32 = 0xFFFFFFFF ^ bitMask
	d.Min = rune(uint32(d.Min) & offsetMask)
	d.Max = rune((uint32(d.Max) + bitMask) & offsetMask)

	d.BitField = make([]uint64, (d.Max-d.Min)>>storageWidth)
	for r, isExtPicto := range d.ExtendedPicto {
		if !isExtPicto {
			continue
		}
		offset := uint32(r-d.Min) >> storageWidth
		bit := uint32(r) & bitMask
		d.BitField[offset] |= 1 << bit
	}

	return writeTo(path, "emoji.tpl", d)
}
*/
