package ansi

import (
	"errors"
	"fmt"
	"io"

	ansi "github.com/leaanthony/go-ansi-parser"
	"github.com/rivo/uniseg"
)

type (
	ColumnOutput struct {
		Columns   []Column
		Separator []byte
		Writer    io.Writer
	}

	Column struct {
		ColumnSpec

		segments []*ansi.StyledText
		buffer   []byte
	}

	ColumnSpec interface {
		Width() int
	}

	FixeColumnSpec int

	DynamicColumnSpec func() int
)

// Compile-time interface checks
var (
	_ io.Writer = (*Column)(nil)

	ErrEmptyColumn = errors.New("empty column")
)

func MakeColumnOutput(writer io.Writer, separator string, specs ...ColumnSpec) (o ColumnOutput) {
	o = ColumnOutput{Separator: []byte(separator), Writer: writer}
	o.Columns = make([]Column, len(specs))
	for i, spec := range specs {
		o.Columns[i] = Column{ColumnSpec: spec}
	}
	return
}

func (o *ColumnOutput) Write(input [][]byte) (n int, err error) {
	if len(input) != len(o.Columns) {
		return 0, fmt.Errorf("data length must matchs the number of columns ")
	}

	for i, col := range o.Columns {
		if _, err = col.Write(input[i]); err != nil {
			return
		}
	}

	numEmpty := 0
	var cn int
	for err == nil && numEmpty < len(o.Columns) {
		numEmpty = 0
		for i, column := range o.Columns {
			if i > 0 {
				if cn, err = o.Writer.Write(o.Separator); err != nil {
					return
				}
				n += cn
			}
			if cn, err = column.WriteLineTo(o.Writer); err != nil {
				return
			}
			n += cn
			if column.IsEmpty() {
				numEmpty++
			}
		}
	}

	return
}

func (c *Column) IsEmpty() bool {
	return false
}

func (c *Column) WriteLineTo(w io.Writer) (n int, err error) {
	c.buffer = c.buffer[:0]
	width := c.Width()
	pos := 0
	for pos < width && len(c.segments) > 0 {
		part, w := c.readUpTo(width - pos)
		if w > 0 {
			c.buffer = append(c.buffer, part...)
			pos += w
		}
	}
	if padding := width - pos; padding > 0 {
		c.buffer = append(c.buffer, nSpaces(padding)...)
	}

	return w.Write(c.buffer)
}

func (c *Column) readUpTo(maxWidth int) (output []byte, width int) {
	if len(c.segments) == 0 {
		return nil, 0
	}
	var reuse bool
	output, width, reuse = readUpTo(c.segments[0], maxWidth)
	if !reuse {
		if len(c.segments) > 0 {
			c.segments = c.segments[1:]
		} else {
			c.segments = nil
		}
	}
	return
}

func readUpTo(segment *ansi.StyledText, maxWidth int) (output []byte, width int, reuse bool) {
	input := []byte(segment.Label)
	rest := input
	var cluster []byte
	var boundaries int
	bytes := 0
	state := -1
	for width < maxWidth && len(rest) > 0 && boundaries&uniseg.MaskLine != uniseg.LineMustBreak {
		cluster, rest, boundaries, state = uniseg.Step(rest, state)
		if cbytes := len(cluster); cbytes > 0 {
			bytes += cbytes
			width++
		}
	}
	if bytes > 0 && boundaries&uniseg.MaskLine == uniseg.LineMustBreak {
		bytes--
	}
	segment.Label = segment.Label[:bytes]
	output = []byte(segment.String())
	if reuse = len(rest) > 0; reuse {
		segment.Label = string(rest)
	}
	return
}

var spaces []byte

func nSpaces(n int) []byte {
	if cap(spaces) < n {
		spaces = make([]byte, n)
		for i := 0; i < n; i++ {
			spaces[i] = ' '
		}
	}
	return spaces[:n]
}

// func padWithSpaces(buf []byte, n int) []byte {
// 	newLen := len(buf) + n
// 	if newLen > cap(buf) {
// 		newBuf := make([]byte, newLen)
// 		copy(newBuf, buf)
// 		buf = newBuf
// 	} else {
// 		buf = buf[:newLen]
// 	}
// 	for i := len(buf); i < newLen; i-- {
// 		buf[i] = ' '
// 	}
// 	return buf
// }

func (c *Column) Write(text []byte) (int, error) {
	if s, err := ansi.Parse(string(text)); err == nil {
		c.segments = append(c.segments, s...)
		return len(text), nil
	} else {
		return 0, err
	}
}

func (s FixeColumnSpec) Width() int {
	return int(s)
}

func (s DynamicColumnSpec) Width() int {
	return s()
}
