package unigrapheme

import "unicode/utf8"

//go:generate go run ./generate

// See https://www.unicode.org/reports/tr29/#Grapheme_Cluster_Boundary_Rules

// StateMachine allows to detect grapheme cluster boundaries in a flow of Unicode runes.
type StateMachine struct {
	previous clusterBoundaryProperty
	size     int
	riCount  int
	expSeq   bool
}

// EOTRune is an artificial rune used to signal the "End Of Text".
const EOTRune = rune(-1)

// Step feeds the state machine with the next rune.
// Returns (size, true) at the end of a cluster, else return (0, false).
func (m *StateMachine) Step(r rune) (size int, isClusterBoundary bool) {
	previous := m.previous
	m.previous = clusterBoundaryProperties.of(r)
	if m.transition(previous, m.previous) {
		size = m.size
		m.size = 0
	}
	if rs := utf8.RuneLen(r); rs > 0 {
		m.size += rs
	}
	return size, size > 0
}

func (m *StateMachine) transition(p, n clusterBoundaryProperty) (isClusterBoundary bool) {

	// [11.0] ExtPict Extend* ZWJ × ExtPict
	if p == exp && (n == ext || n == zwj) {
		m.expSeq = true
	} else if m.expSeq && p != ext && p != zwj {
		m.expSeq = false
	}

	// [12.0] ^     (RI RI)* RI × RI
	// [13.0] [^RI] (RI RI)* RI × RI
	if p == ri_ {
		m.riCount++
	} else {
		m.riCount = 0
	}

	switch {

	case // [0.2] sot ÷ *
		p == sot:
		// According to the Unicode rules, this should return true.
		// However, even if it makes sense, no one wants to start with
		// an empty cluster at the start of every strings.
		return false

	case // [0.3] * ÷ eot
		n == eot:
		return true

	case // [3.0] CR × LF
		p == cr_ && n == lf_:
		return false

	case // [4.0] ( Control | CR | LF ) ÷ *
		p == ctl, p == cr_, p == lf_,
		// [5.0] * ÷ ( Control | CR | LF )
		n == ctl, n == cr_, n == lf_:

		return true

	case // [6.0] L × ( L | V | LV | LVT )
		p == l__ && (n == l__ || n == v__ || n == lv_ || n == lvt),
		// [7.0] ( LV | V ) × ( V | T )
		(p == lv_ || p == v__) && (n == v__ || n == t__),
		// [8.0] ( LVT | T ) × T
		(p == lvt || p == t__) && n == t__,
		// [9.0] * × (Extend | ZWJ)
		n == ext, n == zwj,
		// [9.1] * × SpacingMark
		n == spm,
		// [9.2] Prepend × *
		p == pre,
		// [11.0] ExtPict Extend* ZWJ × ExtPict
		m.expSeq && p == zwj && n == exp,
		// [12.0] ^     (RI RI)* RI × RI
		// [13.0] [^RI] (RI RI)* RI × RI
		m.riCount&1 == 1 && n == ri_:

		return false

	// [999.0] * ÷ *
	default:
		return true
	}
}
