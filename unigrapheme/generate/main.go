package main

import (
	"context"
	"log"
	"path/filepath"
	"runtime"
	"time"
)

const (
	emojiDataURL = "https://www.unicode.org/Public/14.0.0/ucd/emoji/emoji-data.txt"
	propertyURL  = "https://www.unicode.org/Public/14.0.0/ucd/auxiliary/GraphemeBreakProperty.txt"
	testURL      = "https://www.unicode.org/Public/14.0.0/ucd/auxiliary/GraphemeBreakTest.txt"
)

var (
	// emojiDataPath = there() + "/../extendedpictographic.go"
	propertyPath = there() + "/../properties.go"
	testPath     = there() + "/../testcases_test.go"
)

func main() {
	ctx, stop := context.WithTimeout(context.Background(), 30*time.Second)
	defer stop()

	properties, err := readPropertiesFrom(ctx, propertyURL)
	if err != nil {
		log.Fatalf("could not read from %s: %s", properties.URL, err)
	}

	emojiData, err := readEmojiDataFrom(ctx, emojiDataURL)
	if err != nil {
		log.Fatalf("could not read from %s: %s", emojiData.URL, err)
	}

	testData, err := readTestsFrom(ctx, testURL)
	if err != nil {
		log.Fatalf("could not read from %s: %s", testData.URL, err)
	}

	for r := range emojiData.ExtendedPicto {
		if p, found := properties.Properties[r]; !found {
			properties.Properties[r] = "exp"
		} else {
			log.Printf("already have a property: %c (%x) => %s", r, r, p)
		}
	}

	if err := writePropertiesTo(propertyPath, properties); err != nil {
		log.Fatalf("could not write to %s: %s", propertyPath, err)
	}

	if err := writeTestsTo(testPath, testData); err != nil {
		log.Fatalf("could not write to %s: %s", testPath, err)
	}
}

func there() string {
	_, myself, _, _ := runtime.Caller(0)
	return filepath.Dir(myself)
}
