package unigrapheme_test

import (
	"testing"
	"unicode/utf8"

	"github.com/stretchr/testify/require"
	"gitlab.com/adirelle/go-libs/unigrapheme"
)

func TestStateMachine(t *testing.T) {
	testCases.Run(t, func(t *testing.T, c TestCase) {
		require := require.New(t)

		var m unigrapheme.StateMachine

		var clusters [][]byte
		l := len(c.Input)
		runeIndex := 0
		clIndex := 0

		for runeIndex <= l {
			r, size := utf8.DecodeRune(c.Input[runeIndex:])
			runeIndex += size
			if r == utf8.RuneError && size == 0 {
				runeIndex++
				r = unigrapheme.EOTRune
			}
			if clSize, isCluster := m.Step(r); isCluster {
				clusters = append(clusters, c.Input[clIndex:clIndex+clSize])
				clIndex += clSize
			}
		}

		require.Equal(c.Clusters, clusters, c)
	})
}

func BenchmarkStateMachine(b *testing.B) {
	testCases.Benchmark(b, func(b *testing.B, c TestCase) {
		var m unigrapheme.StateMachine
		l := len(c.Input)
		runeIndex := 0

		for runeIndex <= l {
			r, size := utf8.DecodeRune(c.Input[runeIndex:])
			runeIndex += size
			if r == utf8.RuneError && size == 0 {
				runeIndex++
				r = unigrapheme.EOTRune
			}
			m.Step(r)
		}
	})
}
