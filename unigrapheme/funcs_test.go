package unigrapheme_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adirelle/go-libs/unigrapheme"
)

func TestWidth(t *testing.T) {
	testCases.Run(t, func(t *testing.T, c TestCase) {
		assert.Equal(t, len(c.Clusters), unigrapheme.Width(c.Input))
	})
}

func BenchmarkWidth(b *testing.B) {
	testCases.Benchmark(b, func(b *testing.B, c TestCase) {
		unigrapheme.Width(c.Input)
	})
}

func TestCut(t *testing.T) {
	testCases.Run(t, func(t *testing.T, c TestCase) {
		assert := assert.New(t)

		input := c.Input
		a, b := unigrapheme.Cut(input, 1)

		w := unigrapheme.Width(input)
		assert.Equal(input, append(a, b...))
		assert.Equal(1, unigrapheme.Width(a), "input=%v a=%v desc=%s", c.Input, a, c.Description)
		assert.Equal(w-1, unigrapheme.Width(b), "input=%v b=%v desc=%s", c.Input, b, c.Description)
	})
}
