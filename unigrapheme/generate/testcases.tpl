{{ template "header.tpl" . }}

var testCases = TestCases{
{{- range .TestCases }}
  {
    LineNumber: {{ .LineNumber }},
    Description: {{ printf "%q" .Description }},
    Input: []byte{
      {{- range .Input -}}
        {{ printf "%#02X" . }},
      {{- end -}}
    },
    Clusters: [][]byte{
      {{- range .Clusters -}}
        {
        {{- range . -}}
          {{- printf "%#02X" . }},
        {{- end -}}
        },
      {{- end -}}
    },
  },
{{- end}}
}
