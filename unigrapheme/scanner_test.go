package unigrapheme_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/adirelle/go-libs/unigrapheme"
)

func TestScanner(t *testing.T) {
	testCases.Run(t, func(t *testing.T, c TestCase) {
		require := require.New(t)

		s := unigrapheme.NewScanner(c.Input)
		var clusters [][]byte
		for {
			cluster, err := s.ReadCluster()
			require.NoError(err)
			if cluster == nil {
				break
			}
			clusters = append(clusters, cluster)
		}
		require.Equal(c.Clusters, clusters)
	})
}
