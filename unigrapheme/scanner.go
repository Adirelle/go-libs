package unigrapheme

import (
	"bufio"
	"io"
	"unicode/utf8"
)

type (
	// Generic interface
	ClusterReader interface {
		ReadCluster() (cluster []byte, err error)
	}

	// A zero-copy, zero-allocation scanner of grapheme cluster.
	Scanner struct {
		input []byte
		m     StateMachine
		index int
	}

	// A lightweight reader of grapheme cluster.
	Reader struct {
		input   io.RuneReader
		m       StateMachine
		buffer  []byte
		cluster []byte
	}
)

// Compile-time interface checks
var (
	_ ClusterReader = (*Scanner)(nil)
	_ ClusterReader = (*Reader)(nil)
)

// NewReader creates a grapheme cluster scanner from the given byte slices.
func NewScanner(input []byte) *Scanner {
	return &Scanner{input: input}
}

// ReadCluster returns the next grapheme cluster from the buffer,
// or nil if the end of the input has been reached.
// The returned slice is a sub-slice of the input buffer.
// Never returns any error.
func (s *Scanner) ReadCluster() (cluster []byte, err error) {
	for cluster == nil && s.index >= 0 {
		r, n := utf8.DecodeRune(s.input[s.index:])
		if r == utf8.RuneError && n == 0 {
			r = EOTRune
			n = -(s.index + 4*utf8.UTFMax)
		}
		pos := s.index
		s.index += n
		if size, found := s.m.Step(r); found {
			cluster = s.input[pos-size : pos]
		}
	}
	return
}

// NewReader creates a grapheme cluster reader from the given uptream reader.
func NewReader(rd io.Reader) (r *Reader) {
	r = &Reader{}
	if rr, ok := rd.(io.RuneReader); ok {
		r.input = rr
	} else {
		r.input = bufio.NewReader(rd)
	}
	r.buffer = make([]byte, 0, 128)
	r.cluster = make([]byte, 0, 128)
	return
}

// ReadCluster returns the next grapheme cluster from its uptream reader,
// or nil if the end of the stream has been reached.
// The content of the returned byte slice is only valid up to the next call to ReadCluster.
// Returns any error returned by uptream reader but io.EOF.
func (s *Reader) ReadCluster() (cluster []byte, err error) {
	for cluster == nil && err == nil {
		var r rune
		r, _, err = s.input.ReadRune()
		if err == io.EOF {
			r, _ = EOTRune, 0
		} else if err != nil {
			return
		}
		if size, found := s.m.Step(r); found {
			cluster = s.buffer[:size]
			s.buffer = s.cluster[:0]
			s.cluster = cluster[:0]
		}
		s.buffer = utf8.AppendRune(s.buffer, r)
	}
	return
}
