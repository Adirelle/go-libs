package main

import (
	"context"
	"io"
	"log"
	"net/http"
	"time"
)

type (
	FetchInfo struct {
		URL          string
		LastModified time.Time
		Now          time.Time
	}
)

func fetch(ctx context.Context, url string) (info FetchInfo, rd io.ReadCloser, err error) {
	info.URL = url
	info.Now = time.Now()

	info.LastModified, rd, err = get(ctx, url)

	return
}

func get(ctx context.Context, url string) (lastModified time.Time, rd io.ReadCloser, err error) {
	var req *http.Request
	if req, err = http.NewRequestWithContext(ctx, http.MethodGet, url, nil); err != nil {
		return
	}

	log.Printf("fetching %s", url)
	var resp *http.Response
	if resp, err = http.DefaultClient.Do(req); err != nil {
		return
	}

	if value, found := resp.Header["Last-Modified"]; found {
		lastModified, err = time.Parse(time.RFC1123, value[0])
		if err != nil {
			log.Printf("ignoring Last-Modified (%q): %s", value, err)
			lastModified = time.Now()
			err = nil
		}
	}

	log.Printf("status: %s", resp.Status)
	rd = resp.Body

	return
}
