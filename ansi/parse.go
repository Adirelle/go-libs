package ansi

import (
	"regexp"
	"strings"
)

// Regular expression to match c0ontrol sequences
var CSRegexp = regexp.MustCompile("^\x1b\\[[0-?]*[ /]*[@-~]")

// Regular expression to match SGR sequences
var SGRRegexp = regexp.MustCompile("^\x1b\\[(\\d+(?:;\\d+)*)?m")

// Return true if data starts with the Control Sequence Introducer (ESC [)
func StartsControlSequence(data []byte) bool {
	return len(data) >= len(CSI) && data[0] == CSI[0] && data[1] == CSI[1]
}

func FindSGR(input []byte) (rest []byte, modifiers []StyleModifier, err error) {
	matches := SGRRegexp.FindSubmatch(input)
	if matches == nil {
		rest = input
		return
	}
	rest = input[len(matches[0]):]
	paramPart := matches[1]
	if len(paramPart) == 0 {
		modifiers = []StyleModifier{Reset}
		return
	}

	var mod StyleModifier
	params := strings.Split(string(paramPart), SGRSeparator)
	for len(params) > 0 && err == nil {
		builder, found := builders[params[0]]
		if !found {
			params = params[1:]
			continue
		}
		mod, params, err = builder(params)
		if mod != nil {
			modifiers = append(modifiers, mod)
		}
	}
	return
}
