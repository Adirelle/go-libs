package unigrapheme

type (
	clusterBoundaryProperty    int
	clusterBoundaryPropertyMap map[rune]clusterBoundaryProperty
)

// The different properties
const (
	sot clusterBoundaryProperty = iota
	oth
	ctl
	cr_
	lf_
	l__
	lv_
	lvt
	t__
	v__
	ext
	pre
	ri_
	spm
	zwj
	exp
	eot
)

var propertyLabels = map[clusterBoundaryProperty]string{
	oth: "Other",
	cr_: "CR",
	ctl: "Control",
	ext: "Extend",
	l__: "L",
	lf_: "LF",
	lv_: "LV",
	lvt: "LVT",
	pre: "Prepend",
	ri_: "Regional_Indicator",
	spm: "SpacingMark",
	t__: "T",
	v__: "V",
	zwj: "ZWJ",
	exp: "ExtPict",
	sot: "StartOfText",
	eot: "EndOfText",
}

func (p clusterBoundaryProperty) GoString() string {
	if l, ok := propertyLabels[p]; ok {
		return l
	}
	panic("unknown value of clusterBoundaryProperty")
}

func (m clusterBoundaryPropertyMap) of(r rune) clusterBoundaryProperty {
	if p, found := m[r]; found {
		return p
	}
	return oth
}
