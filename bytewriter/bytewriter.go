// bytewriter provides Writer, a glorified byte copier.
package bytewriter

import (
	"errors"
	"io"
)

// Writer implements io.Writer, io.ByteWriter, io.StringWriter and io.ReaderFrom.
// around copy. It keeps the semantic of copy: it never writes past the slice length,
// nor does it reallocate it. Upon reaching the slice end, Writer methods return ErrBufferFull.
type Writer struct {
	buffer []byte
	pos    int
}

var (
	// ErrBufferFull is returned when trying to write past the end of the buffer.
	ErrBufferFull = errors.New("buffer is full")

	_ io.Writer       = (*Writer)(nil)
	_ io.ByteWriter   = (*Writer)(nil)
	_ io.StringWriter = (*Writer)(nil)
	_ io.ReaderFrom   = (*Writer)(nil)
)

// New creates a new Writer to write in the given byte slice.
func New(buffer []byte) *Writer {
	return &Writer{buffer: buffer}
}

// Reset allows to reuse an existing Writer with another byte slice.
func (w *Writer) Reset(buffer []byte) {
	w.buffer = buffer
	w.pos = 0
}

// IsFull returns true when the Writer has reached the end of the slice.
func (w *Writer) IsFull() bool {
	return w.pos == len(w.buffer)
}

// IsEmpty returns true when the Writer is at the start of the slice.
func (w *Writer) IsEmpty() bool {
	return w.pos == 0
}

// Len returns the number of bytes written so far.
func (w *Writer) Len() int {
	return w.pos
}

// Cap returns the length of the internal buffer.
func (w *Writer) Cap() int {
	return len(w.buffer)
}

// Bytes returns a slice of the bytes written so far. It is only
// valid until the next call to a writing method.
func (w *Writer) Bytes() []byte {
	return w.buffer[:w.pos]
}

// Write implements io.Writer
func (w *Writer) Write(data []byte) (n int, err error) {
	n = copy(w.buffer[w.pos:], data)
	w.pos += n
	if n < len(data) {
		err = ErrBufferFull
	}
	return
}

// WriteString implements io.StringWriter
func (w *Writer) WriteString(str string) (n int, err error) {
	n = copy(w.buffer[w.pos:], str)
	w.pos += n
	if n < len(str) {
		err = ErrBufferFull
	}
	return
}

// WriteString implements io.ByteWriter
func (w *Writer) WriteByte(data byte) error {
	if w.pos >= len(w.buffer) {
		return ErrBufferFull
	}
	w.buffer[w.pos] = data
	w.pos++
	return nil
}

// WriteString implements io.ReaderFrom
func (w *Writer) ReadFrom(r io.Reader) (n int64, err error) {
	var step int
	for err == nil {
		step, err = r.Read(w.buffer[w.pos:])
		w.pos += step
		n += int64(step)
		if err == nil && step == 0 && w.pos == len(w.buffer) {
			err = ErrBufferFull
		}
	}
	if err == io.EOF {
		err = nil
	}
	return
}
