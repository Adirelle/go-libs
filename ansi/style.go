package ansi

import (
	"errors"
	"fmt"
	"strings"
)

type (
	// A Style is a collection of Attribute
	Style struct {
		Attributes []Attribute
	}

	// An Attribute holds one a Parameter for a given Slot
	Attribute struct {
		Slot      AttributeSlot
		Parameter string
	}

	AttributeSlot int

	// A StyleModifier can be applied to a Style to modify it
	StyleModifier interface {
		attributeLike
		Apply(*Style) bool
	}

	StyleModifiers []StyleModifier

	attributeLike interface {
		fmt.Stringer
		slot() AttributeSlot
	}

	attributeAdder   struct{ Attribute }
	attributeRemover struct{ Attribute }
	styleResetter    struct{}

	builderFunc func(params []string) (StyleModifier, []string, error)
)

// Supported attribute slots
const (
	ForegroundSlot AttributeSlot = 1 << iota
	BackgroundSlot
	WeightSlot
	ItalicSlot
	UnderlineSlot
	BlinkSlot
	ReverseSlot
	ConcealSlot
	StrikeSlot

	AllSlots = (1 << iota) - 1
)

const (
	// Control Sequence Introducer
	CSI = "\x1b["

	// SGR parameter separator
	SGRSeparator = ";"

	// SGR final byte
	SGRType = "m"

	// First value of a an extended foreground color SGR
	FgExtendedCode = "38"

	// First value of a an extended background color SGR
	BgExtendedCode = "48"

	// Value of the second parameter in extended color SGR for 8-bit colors
	ExIndexedColorCode = "5"

	// Value of the second parameter in extended color SGR for 24-bit colors
	ExRGBColorCode = "2"

	// Shortand for the reset SGR
	ResetSGR = CSI + SGRType
)

var builders = make(map[string]builderFunc)

// Supported ANSI SGR codes
var (
	Reset           = register(styleResetter{})
	Bold            = register(WeightSlot.adder("1"))
	Faint           = register(WeightSlot.adder("2"))
	Italic          = register(ItalicSlot.adder("3"))
	Underline       = register(UnderlineSlot.adder("4"))
	SlowBlink       = register(BlinkSlot.adder("5"))
	RapidBlink      = register(BlinkSlot.adder("6"))
	Reverse         = register(ReverseSlot.adder("7"))
	Conceal         = register(ConcealSlot.adder("8"))
	Strike          = register(StrikeSlot.adder("9"))
	ResetWeight     = register(WeightSlot.remover("22"))
	ResetItalic     = register(ItalicSlot.remover("23"))
	ResetUnderline  = register(UnderlineSlot.remover("24"))
	ResetBlink      = register(BlinkSlot.remover("25"))
	ResetReverse    = register(ReverseSlot.remover("27"))
	ResetConceal    = register(ConcealSlot.remover("28"))
	ResetStrike     = register(StrikeSlot.remover("29"))
	FgBlack         = register(ForegroundSlot.adder("30"))
	FgRed           = register(ForegroundSlot.adder("31"))
	FgGreen         = register(ForegroundSlot.adder("32"))
	FgYellow        = register(ForegroundSlot.adder("33"))
	FgBlue          = register(ForegroundSlot.adder("34"))
	FgMagenta       = register(ForegroundSlot.adder("35"))
	FgCyan          = register(ForegroundSlot.adder("36"))
	FgWhite         = register(ForegroundSlot.adder("37"))
	ResetForeground = register(ForegroundSlot.remover("39"))
	BgBlack         = register(BackgroundSlot.adder("40"))
	BgRed           = register(BackgroundSlot.adder("41"))
	BgGreen         = register(BackgroundSlot.adder("42"))
	BgYellow        = register(BackgroundSlot.adder("43"))
	BgBlue          = register(BackgroundSlot.adder("44"))
	BgMagenta       = register(BackgroundSlot.adder("45"))
	BgCyan          = register(BackgroundSlot.adder("46"))
	BgWhite         = register(BackgroundSlot.adder("47"))
	ResetBackground = register(BackgroundSlot.remover("49"))
	FgHiBlack       = register(ForegroundSlot.adder("90"))
	FgHiRed         = register(ForegroundSlot.adder("91"))
	FgHiGreen       = register(ForegroundSlot.adder("92"))
	FgHiYellow      = register(ForegroundSlot.adder("93"))
	FgHiBlue        = register(ForegroundSlot.adder("94"))
	FgHiMagenta     = register(ForegroundSlot.adder("95"))
	FgHiCyan        = register(ForegroundSlot.adder("96"))
	FgHiWhite       = register(ForegroundSlot.adder("97"))
	BgHiBlack       = register(BackgroundSlot.adder("100"))
	BgHiRed         = register(BackgroundSlot.adder("101"))
	BgHiGreen       = register(BackgroundSlot.adder("102"))
	BgHiYellow      = register(BackgroundSlot.adder("103"))
	BgHiBlue        = register(BackgroundSlot.adder("104"))
	BgHiMagenta     = register(BackgroundSlot.adder("105"))
	BgHiCyan        = register(BackgroundSlot.adder("106"))
	BgHiWhite       = register(BackgroundSlot.adder("107"))
)

var ErrInvalidExtendedColor = errors.New("invalid extended color")

func init() {
	builders[FgExtendedCode] = extendedForeground
	builders[BgExtendedCode] = extendedBackground
}

func register(modifier StyleModifier) StyleModifier {
	builders[modifier.String()] = func(p []string) (StyleModifier, []string, error) {
		return modifier, p[1:], nil
	}
	return modifier
}

func (s AttributeSlot) adder(param string) StyleModifier {
	return &attributeAdder{Attribute{s, param}}
}

func (s AttributeSlot) remover(param string) StyleModifier {
	return &attributeRemover{Attribute{s, param}}
}

func (s AttributeSlot) contains(other AttributeSlot) bool {
	return s&other == other
}

func extendedForeground(params []string) (StyleModifier, []string, error) {
	if len(params) >= 3 && params[1] == ExIndexedColorCode {
		return ForegroundSlot.adder(strings.Join(params[:3], SGRSeparator)), params[3:], nil
	}
	if len(params) >= 5 && params[1] == ExRGBColorCode {
		return ForegroundSlot.adder(strings.Join(params[:5], SGRSeparator)), params[5:], nil
	}
	return nil, params, ErrInvalidExtendedColor
}

func extendedBackground(params []string) (StyleModifier, []string, error) {
	if len(params) >= 3 && params[1] == ExIndexedColorCode {
		return BackgroundSlot.adder(strings.Join(params[:3], SGRSeparator)), params[3:], nil
	}
	if len(params) >= 5 && params[1] == ExRGBColorCode {
		return BackgroundSlot.adder(strings.Join(params[:5], SGRSeparator)), params[5:], nil
	}
	return nil, params, ErrInvalidExtendedColor
}

const (
	indexedForegroundFmt = FgExtendedCode + SGRSeparator + ExIndexedColorCode + SGRSeparator + "%d"
	rgbForegroundFmt     = FgExtendedCode + SGRSeparator + ExRGBColorCode + SGRSeparator + "%d" + SGRSeparator + "%d" + SGRSeparator + "%d"
	indexedBackgroundFmt = BgExtendedCode + SGRSeparator + ExIndexedColorCode + SGRSeparator + "%d"
	rgbBackgroundFmt     = BgExtendedCode + SGRSeparator + ExRGBColorCode + SGRSeparator + "%d" + SGRSeparator + "%d" + SGRSeparator + "%d"
)

// Create a StyleModifier to set the foreground to the color of the given index
func FgIndexedColor(color byte) StyleModifier {
	return ForegroundSlot.adder(fmt.Sprintf(indexedForegroundFmt, color))
}

// Create a StyleModifier to set the foreground to the given RGB color
func FgRGBColor(r, g, b byte) StyleModifier {
	return ForegroundSlot.adder(fmt.Sprintf(rgbForegroundFmt, r, g, b))
}

// Create a StyleModifier to set the background to the given color index
func BgIndexedColor(color byte) StyleModifier {
	return BackgroundSlot.adder(fmt.Sprintf(indexedBackgroundFmt, color))
}

// Create a StyleModifier to set the background to the given RGB color
func BgRGBColor(r, g, b byte) StyleModifier {
	return BackgroundSlot.adder(fmt.Sprintf(rgbBackgroundFmt, r, g, b))
}

func (a Attribute) String() string {
	return a.Parameter
}

func (a Attribute) slot() AttributeSlot {
	return a.Slot
}

// Create a new Style from the given Modifiers
func NewStyle(mods ...StyleModifier) *Style {
	s := &Style{}
	s.Apply(mods...)
	return s
}

// Normal returns a default style.
func Normal() *Style {
	return &Style{}
}

func (s Style) Clone() *Style {
	s2 := &Style{make([]Attribute, len(s.Attributes))}
	copy(s2.Attributes, s.Attributes)
	return s2
}

// Sprint returns the given string surroundd
func (s Style) With(mods ...StyleModifier) *Style {
	s2 := s.Clone()
	s2.Apply(mods...)
	return s2
}

// IsBlank returns true when the style contains no Attribute
func (s Style) IsBlank() bool {
	return len(s.Attributes) == 0
}

// Strings returns a full SGR representing all the Attributes
func (s Style) String() string {
	if s.IsBlank() {
		return ResetSGR
	}
	return FormatSGR(s.Attributes)
}

const sgrFmt = CSI + "%s" + SGRType

func FormatSGR[T attributeLike](attrs []T) string {
	if len(attrs) == 1 && attrs[0].String() == "0" {
		return ResetSGR
	}
	params := make([]string, len(attrs))
	for i, a := range attrs {
		params[i] = a.String()
	}
	return fmt.Sprintf(sgrFmt, strings.Join(params, SGRSeparator))
}

// Sprint returns the given string surroundd
func (s Style) Sprint(data string) string {
	if s.IsBlank() {
		return data
	}
	return fmt.Sprintf("%s%s%s", s.String(), data, Normal().String())
}

// Sprint returns the given string surroundd
func (s Style) Sprintf(tpl string, values ...any) string {
	return s.Sprint(fmt.Sprintf(tpl, values...))
}

// Apply applies the given Modifiers to the Style.
func (s *Style) Apply(mods ...StyleModifier) {
	for _, mod := range mods {
		mod.Apply(s)
	}
}

// ApplyAndFilter applies the given Modifiers to the Style.
// It returns the Modifiers that had an effect.
func (s *Style) ApplyAndFilter(mods ...StyleModifier) (result []StyleModifier) {
	result = mods[:]
	for _, mod := range mods {
		if mod.Apply(s) {
			result, _ = addExclusive(result, mod)
		}
	}
	return
}

func addExclusive[T attributeLike](items []T, item T) (result []T, added bool) {
	slot := item.slot()
	result = items[:0]
	found := false
	for _, current := range items {
		if curSlot := current.slot(); !found && curSlot == slot && current.String() == item.String() {
			result = append(result, current)
			found = true
		} else if !slot.contains(curSlot) {
			result = append(result, current)
		}
	}
	if !found {
		result = append(result, item)
		added = true
	}
	return
}

func clearSlot[T attributeLike](items []T, slot AttributeSlot) (result []T, removed bool) {
	result = items[:0]
	for _, current := range items {
		if !slot.contains(current.slot()) {
			result = append(result, current)
		} else {
			removed = true
		}
	}
	return
}

func (a *attributeAdder) Apply(s *Style) (added bool) {
	s.Attributes, added = addExclusive(s.Attributes, a.Attribute)
	return
}

func (a *attributeAdder) String() string {
	return a.Attribute.Parameter
}

func (r *attributeRemover) Apply(s *Style) (removed bool) {
	s.Attributes, removed = clearSlot(s.Attributes, r.Slot)
	return
}

func (r *attributeRemover) String() string {
	return r.Attribute.Parameter
}

func (styleResetter) Apply(s *Style) bool {
	if len(s.Attributes) > 0 {
		s.Attributes = nil
		return true
	}
	return false
}

func (styleResetter) String() string {
	return "0"
}

func (styleResetter) slot() AttributeSlot {
	return AllSlots
}
