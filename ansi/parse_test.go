package ansi_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adirelle/go-libs/ansi"
)

func TestStartsControlSequence(t *testing.T) {
	assert := assert.New(t)

	assert.False(ansi.StartsControlSequence(nil))
	assert.False(ansi.StartsControlSequence([]byte("aaa")))
	assert.False(ansi.StartsControlSequence([]byte("\x1b")))
	assert.True(ansi.StartsControlSequence([]byte("\x1b[")))
}

type findSGRTestCase struct {
	input string

	expRest string
	expMod  string
	expErr  bool
}

func (c findSGRTestCase) String() string {
	return fmt.Sprintf("(%q,%q,%q,%v)", c.input, c.expRest, c.expMod, c.expErr)
}

func (c findSGRTestCase) Run(t *testing.T) {
	assert := assert.New(t)

	rest, mod, err := ansi.FindSGR([]byte(c.input))
	if c.expErr {
		assert.Error(err)
	} else {
		assert.NoError(err)
	}
	assert.Equal(c.expRest, string(rest))
	assert.Equal(c.expMod, join(mod))
}

func TestFindSGR(t *testing.T) {
	RunSubTests(t,
		[]findSGRTestCase{
			{"\x1b[mfoo", "foo", "0", false},
			{"\x1b[0mfoo", "foo", "0", false},
			{"\x1b[1mfoo", "foo", "1", false},
			{"\x1b[1;0mfoo", "foo", "1;0", false},
			{"\x1b[38;5;8mfoo", "foo", "38;5;8", false},
			{"\x1b[38;2;0;1;2mfoo", "foo", "38;2;0;1;2", false},
			{"\x1b[48;5;8mfoo", "foo", "48;5;8", false},
			{"\x1b[48;2;0;1;2mfoo", "foo", "48;2;0;1;2", false},

			// Not a SGR at all
			{"foobar", "foobar", "", false},

			// Not a valid SGR
			{"\x1b[2,4m", "\x1b[2,4m", "", false},

			// Invalid extended foreground colors
			{"\x1b[38;3mfoo", "foo", "", true},
			{"\x1b[38;2;8;4mfoo", "foo", "", true},
			{"\x1b[38;5mfoo", "foo", "", true},
		})
}
