package unigrapheme_test

import (
	"fmt"
	"runtime"
	"strings"
	"sync"
	"testing"
)

type (
	TestCases []TestCase

	TestCase struct {
		LineNumber  int
		Description string
		Input       []byte
		Clusters    [][]byte
	}
)

func (c TestCase) Name() string {
	return fmt.Sprintf("%d", c.LineNumber)
}

func (c TestCase) StringInput() string {
	return string(c.Input)
}

func (c TestCase) ByteInput() []byte {
	return c.Input
}

func (c TestCase) Reader() *strings.Reader {
	return strings.NewReader(c.StringInput())
}

func (cs TestCases) Run(t *testing.T, test func(t *testing.T, c TestCase)) {
	t.Helper()
	for _, c := range cs {
		t.Run(c.Name(), func(t *testing.T) {
			t.Helper()
			test(t, c)
		})
	}
}

func (cs TestCases) Benchmark(b *testing.B, test func(b *testing.B, c TestCase)) {
	b.Helper()
	sem := make(chan struct{}, runtime.NumCPU())
	var wg sync.WaitGroup
	for _, c := range cs {
		b.Run(c.Name(), func(b *testing.B) {
			b.Helper()
			n := b.N / len(cs)
			wg.Add(1)
			go func() {
				sem <- struct{}{}
				defer func() {
					<-sem
					wg.Done()
				}()
				for i := 0; i < n; i++ {
					test(b, c)
				}
			}()
		})
	}
	wg.Wait()
}
