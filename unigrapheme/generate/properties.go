package main

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"log"
	"unicode/utf8"
)

type (
	Properties struct {
		FetchInfo
		Package    string
		Properties map[rune]string
	}
)

var propNameMap = map[string]string{
	"Other":              "oth",
	"CR":                 "cr_",
	"Control":            "ctl",
	"Extend":             "ext",
	"L":                  "l__",
	"LF":                 "lf_",
	"LV":                 "lv_",
	"LVT":                "lvt",
	"Prepend":            "pre",
	"Regional_Indicator": "ri_",
	"SpacingMark":        "spm",
	"T":                  "t__",
	"V":                  "v__",
	"ZWJ":                "zwj",
}

func readPropertiesFrom(ctx context.Context, url string) (p Properties, err error) {
	var input io.ReadCloser
	if p.FetchInfo, input, err = fetch(ctx, url); err != nil {
		return
	}
	defer input.Close()

	p.Package = "unigrapheme"
	p.Properties = make(map[rune]string, utf8.MaxRune)

	lines := bufio.NewScanner(input)
	lineCount := 0
	for lines.Scan() {
		lineCount++
		var from, to rune
		var category string

		if n, err := fmt.Sscanf(lines.Text(), "%x..%x ; %s ", &from, &to, &category); err == nil && n == 3 {
			// OK
		} else if n, err := fmt.Sscanf(lines.Text(), "%x ; %s ", &from, &category); err == nil && n == 2 {
			to = from
		} else {
			continue
		}

		for ; from <= to; from++ {
			p.Properties[from] = propNameMap[category]
		}
	}

	log.Printf("read %d lines, %d properties", lineCount, len(p.Properties))

	return
}

func writePropertiesTo(path string, p Properties) error {
	return writeTo(path, "properties.tpl", p)
}
