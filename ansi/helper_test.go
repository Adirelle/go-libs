package ansi_test

import (
	"fmt"
	"strings"
	"testing"

	"gitlab.com/adirelle/go-libs/ansi"
)

func join[T fmt.Stringer](items []T) string {
	strs := make([]string, len(items))
	for i, item := range items {
		strs[i] = item.String()
	}
	return strings.Join(strs, ansi.SGRSeparator)
}

type (
	subTest interface {
		fmt.Stringer
		Run(*testing.T)
	}
)

func RunSubTests[T subTest](t *testing.T, subTests []T) {
	t.Helper()
	for _, subTest := range subTests {
		t.Run(subTest.String(), subTest.Run)
	}
}

func RunSubTestv[T subTest](t *testing.T, subTests ...T) {
	t.Helper()
	RunSubTests(t, subTests)
}
