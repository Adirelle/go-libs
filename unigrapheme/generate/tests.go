package main

import (
	"bufio"
	"context"
	"io"
	"log"
	"regexp"
	"strconv"
	"unicode/utf8"
)

type (
	TestData struct {
		FetchInfo
		Package   string
		TestCases []TestCase
	}

	TestCase struct {
		LineNumber  int
		Description string
		Input       []byte
		Clusters    [][]byte
	}
)

var (
	testRe = regexp.MustCompile(`^([÷×](?:\s+[0-9A-Z]+\s+[÷×])*)\s*#\s*(.*)\s*$`)
	partRe = regexp.MustCompile(`\s*([0-9A-Z]+)?\s*([÷×])\s*`)
)

func readTestsFrom(ctx context.Context, url string) (d TestData, err error) {
	var input io.ReadCloser
	if d.FetchInfo, input, err = fetch(ctx, url); err != nil {
		return
	}
	defer input.Close()

	d.Package = "unigrapheme_test"

	lines := bufio.NewScanner(input)
	lineCount := 0
	for lines.Scan() {
		lineCount++

		match := testRe.FindStringSubmatch(lines.Text())
		if match == nil {
			continue
		}

		c := TestCase{LineNumber: lineCount, Description: match[2]}

		parts := partRe.FindAllStringSubmatch(match[1], -1)

		var current []byte
		for _, part := range parts {
			if part[1] != "" {
				r, err := strconv.ParseInt(part[1], 16, 32)
				if err != nil {
					panic(err)
				}
				current = utf8.AppendRune(current, rune(r))
				c.Input = utf8.AppendRune(c.Input, rune(r))
			}
			if part[2] == "÷" && len(current) > 0 {
				c.Clusters = append(c.Clusters, current)
				current = nil
			}
		}

		d.TestCases = append(d.TestCases, c)
	}

	log.Printf("read %d lines, found %d test cases", lineCount, len(d.TestCases))

	return
}

func writeTestsTo(path string, d TestData) error {
	return writeTo(path, "testcases.tpl", d)
}
