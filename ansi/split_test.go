package ansi_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adirelle/go-libs/ansi"
)

type (
	splitTestCase struct {
		input    string
		style    *ansi.Style
		maxWidth int

		expOutput []string
		expErr    error
	}
)

func mksts(input string, style *ansi.Style, maxWidth int, output ...string) *splitTestCase {
	return &splitTestCase{input, style, maxWidth, output, nil}
}

func (c *splitTestCase) String() string {
	return fmt.Sprintf("(%q,%q,%d)", c.input, c.style.String(), c.maxWidth)
}

func (c *splitTestCase) Run(t *testing.T) {
	assert := assert.New(t)

	splitter := ansi.NewSplitter([]byte(c.input), c.maxWidth)
	splitter.Style = c.style

	var output []string
	var line []byte
	var err error
	hasMore := true

	for hasMore && err == nil {
		line, hasMore, err = splitter.ReadLine()
		if err == nil {
			output = append(output, string(line))
		}
	}

	assert.Equal(c.expErr, err)
	assert.Equal(c.expOutput, output)
}

func TestSplit(t *testing.T) {
	RunSubTestv(t,
		mksts("", ansi.Normal(), 5, ""),
		mksts("foobar", ansi.Normal(), 10, "foobar"),
		mksts("foobar", ansi.Normal(), 5, "fooba", "r"),
		mksts("foobar", ansi.Normal(), 2, "fo", "ob", "ar"),
		mksts("été", ansi.Normal(), 2, "ét", "é"),
		mksts("\u200dzz", ansi.Normal(), 2, "\u200dz", "z"),
		mksts("\x1b[1mbold\x1b[m", ansi.Normal(), 20, "\x1b[1mbold\x1b[m"),
		mksts("\x1b[1mbold\x1b[m", ansi.Normal(), 10, "\x1b[1mbold\x1b[m"),
		mksts("\x1b[1mbold\x1b[m", ansi.Normal(), 2, "\x1b[1mbo\x1b[m", "\x1b[1mld\x1b[m"),
		mksts("\x1b[1mbold", ansi.Normal(), 2, "\x1b[1mbo\x1b[m", "\x1b[1mld\x1b[m"),
		mksts("aaaabb\nccddee", ansi.Normal(), 4, "aaaa", "bb", "ccdd", "ee"),
		mksts("a\tabbb", ansi.Normal(), 10, "a       ab", "bb"),
	)
}

func BenchmarkSplit(b *testing.B) {
	for i := 0; i < b.N; i++ {
		benchSplit(b)
	}
}

func benchSplit(b *testing.B) {
	s := ansi.NewSplitter([]byte("\x1b[1mbé\x1b[30ml\u200d\taze\nzazed\x1b[m"), 2)
	var err error
	hasMore := true
	for hasMore && err == nil {
		_, hasMore, err = s.ReadLine()
	}
	if err != nil {
		b.Error(err.Error())
	}
}
