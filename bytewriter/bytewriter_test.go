package bytewriter_test

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/adirelle/go-libs/bytewriter"
)

func TestNewBuffer(t *testing.T) {
	assert := assert.New(t)

	b := make([]byte, 5, 10)

	w := bytewriter.New(b)
	assert.Zero(w.Len())
	assert.Equal(5, w.Cap())
	assert.Empty(w.Bytes())
	assert.True(w.IsEmpty())
	assert.False(w.IsFull())
}

func TestWrite(t *testing.T) {
	assert := assert.New(t)

	w := bytewriter.New(make([]byte, 10))

	n, err := w.Write([]byte("01234567"))
	assert.Equal(8, n)
	assert.NoError(err)
	assert.Equal(8, w.Len())
	assert.Equal([]byte("01234567"), w.Bytes())
	assert.False(w.IsEmpty())
	assert.False(w.IsFull())

	n, err = w.Write([]byte("0123"))
	assert.Equal(2, n)
	assert.ErrorIs(err, bytewriter.ErrBufferFull)
	assert.Equal(10, w.Len())
	assert.Equal([]byte("0123456701"), w.Bytes())
	assert.False(w.IsEmpty())
	assert.True(w.IsFull())
}

func TestWriteString(t *testing.T) {
	assert := assert.New(t)

	w := bytewriter.New(make([]byte, 10))

	n, err := w.WriteString("01234567")
	assert.Equal(8, n)
	assert.NoError(err)
	assert.Equal(8, w.Len())
	assert.Equal([]byte("01234567"), w.Bytes())
	assert.False(w.IsEmpty())
	assert.False(w.IsFull())

	n, err = w.WriteString("0123")
	assert.Equal(2, n)
	assert.ErrorIs(err, bytewriter.ErrBufferFull)
	assert.Equal(10, w.Len())
	assert.Equal([]byte("0123456701"), w.Bytes())
	assert.False(w.IsEmpty())
	assert.True(w.IsFull())
}

func TestWriteByte(t *testing.T) {
	assert := assert.New(t)

	w := bytewriter.New(make([]byte, 1))

	err := w.WriteByte('!')
	assert.NoError(err)
	assert.Equal(1, w.Len())
	assert.Equal([]byte("!"), w.Bytes())
	assert.False(w.IsEmpty())
	assert.True(w.IsFull())

	err = w.WriteByte('!')
	assert.ErrorIs(err, bytewriter.ErrBufferFull)
	assert.Equal(1, w.Len())
	assert.Equal([]byte("!"), w.Bytes())
	assert.False(w.IsEmpty())
	assert.True(w.IsFull())
}

func TestReadFrom(t *testing.T) {
	assert := assert.New(t)

	first := strings.NewReader("01234")
	second := strings.NewReader("012345")

	w := bytewriter.New(make([]byte, 10))

	n, err := w.ReadFrom(first)
	assert.NoError(err)
	assert.Equal(int64(5), n)
	assert.Equal(5, w.Len())
	assert.Equal([]byte("01234"), w.Bytes())
	assert.False(w.IsEmpty())
	assert.False(w.IsFull())

	n, err = w.ReadFrom(second)
	assert.ErrorIs(err, bytewriter.ErrBufferFull)
	assert.Equal(int64(5), n)
	assert.Equal(10, w.Len())
	assert.Equal([]byte("0123401234"), w.Bytes())
	assert.False(w.IsEmpty())
	assert.True(w.IsFull())
}

func TestReadFromExact(t *testing.T) {
	assert := assert.New(t)

	first := strings.NewReader("01234")

	w := bytewriter.New(make([]byte, 5))

	n, err := w.ReadFrom(first)
	assert.NoError(err)
	assert.Equal(int64(5), n)
	assert.Equal(5, w.Len())
	assert.Equal([]byte("01234"), w.Bytes())
	assert.False(w.IsEmpty())
	assert.True(w.IsFull())
}

func BenchmarkWriter(b *testing.B) {
	var backing [10]byte
	w := bytewriter.New(backing[:])
	for i := 0; i < b.N; i++ {
		w.Reset(backing[:])
		_, _ = w.WriteString("01234567")
		_, _ = w.WriteString("0123")
	}
}

func FuzzWriter(f *testing.F) {
	f.Add([]byte("01234"), "56789", uint(10))
	f.Fuzz(func(t *testing.T, bytes []byte, str string, size uint) {
		assert := assert.New(t)

		b := make([]byte, size)
		w := bytewriter.New(b)
		assert.Equal(int(size), w.Cap())
		assert.True(w.IsEmpty())

		n, err := w.Write(bytes)
		if err == nil {
			assert.Equal(n, len(bytes))
		} else {
			assert.ErrorIs(err, bytewriter.ErrBufferFull)
			assert.Less(n, len(bytes))
		}
		assert.Equal(n, w.Len())
		assert.Equal(w.Len() == 0, w.IsEmpty())
		assert.Equal(w.Len() == w.Cap(), w.IsFull())
		assert.Equal(bytes[:n], w.Bytes())
		assert.Equal(bytes[:n], b[:n])

		p := n

		n, err = w.WriteString(str)
		if err == nil {
			assert.Equal(n, len(str))
		} else {
			assert.ErrorIs(err, bytewriter.ErrBufferFull)
			assert.Less(n, len(str))
		}
		assert.Equal(n+p, w.Len())
		assert.Equal(w.Len() == 0, w.IsEmpty())
		assert.Equal(w.Len() == w.Cap(), w.IsFull())
		assert.Equal(str[:n], string(w.Bytes()[p:]))
		assert.Equal(str[:n], string(b[p:p+n]))
	})
}
