package main

import (
	"log"
	"os"
	"text/template"
)

var templates = template.Must(
	template.
		New("generate").
		Funcs(template.FuncMap{
			"every": func(n int) *Every {
				return &Every{0, n}
			},
		}).
		ParseGlob(there() + "/*.tpl"),
)

func writeTo(path string, tplName string, data interface{}) (err error) {
	log.Printf("writing %s to %s", tplName, path)

	var output *os.File
	if output, err = os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0o777); err != nil {
		return
	}
	defer output.Close()

	if err = templates.ExecuteTemplate(output, tplName, data); err == nil {
		log.Println("done")
	}

	return
}

type (
	Every struct {
		I int
		N int
	}
)

func (e *Every) Test() bool {
	e.I++
	return (e.I % e.N) == 0
}
