package ansi

import (
	"unicode/utf8"

	"github.com/rivo/uniseg"
)

type (
	Splitter struct {
		Input []byte
		Width int
		*Style

		state int
	}
)

func NewSplitter(input []byte, width int) *Splitter {
	return &Splitter{Input: input, Width: width, Style: Normal(), state: -1}
}

func (s *Splitter) ReadLine() (line []byte, hasMore bool, err error) {
	if s.Width < 1 {
		panic("null or negative width")
	}

	var seq []byte
	var mods []StyleModifier
	startStyle := s.Style.Clone()
	width := 0

loop:
	for err == nil && len(s.Input) > 0 {
		b := s.Input[0]
		switch {
		case b == '\x1b':
			s.Input, mods, err = FindSGR(s.Input)
			if err != nil {
				return nil, false, err
			}
			if mods != nil {
				kept := s.Style.ApplyAndFilter(mods...)
				if len(kept) > 0 {
					line = append(line, FormatSGR(kept)...)
				}
			} else {
				panic("not a SGR")
			}
		case b == '\t':
			if width >= s.Width {
				break loop
			}
			s.Input = s.Input[1:]
			nextTab := (width - width%8) + 8
			for width < nextTab && width < s.Width {
				line = append(line, ' ')
				width++
			}
		case b == '\n':
			s.Input = s.Input[1:]
			break loop
		case b < ' ':
			s.Input = s.Input[1:]
		case width >= s.Width:
			break loop
		case b < utf8.RuneSelf:
			s.Input = s.Input[1:]
			line = append(line, b)
			width++
		default:
			if seq, s.Input, _, s.state = uniseg.Step(s.Input, s.state); seq == nil {
				panic("invalid sequence")
			}
			line = append(line, seq...)
			width++
		}
	}

	if len(line) > 0 {
		if !startStyle.IsBlank() {
			line = append([]byte(startStyle.String()), line...)
		}
		if !s.Style.IsBlank() {
			line = append(line, ResetSGR...)
		}
	}

	hasMore = len(s.Input) > 0

	return
}
