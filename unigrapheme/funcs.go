package unigrapheme

func Width(input []byte) (width int) {
	s := NewScanner(input)
	for {
		if cl, _ := s.ReadCluster(); len(cl) > 0 {
			width++
		} else {
			return
		}
	}
}

func WidthString(input string) (width int) {
	return Width([]byte(input))
}

func Cut(input []byte, width int) (before []byte, after []byte) {
	s := NewScanner(input)
	var column, index int
	for column < width {
		cl, _ := s.ReadCluster()
		l := len(cl)
		if l == 0 {
			break
		}
		column++
		index += l
	}
	return input[:index], input[index:]
}
